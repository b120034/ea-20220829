# 10. Development Tools

Date: 2021-04-06

## Status

Accepted

## Context

1. 개발자간 도구 통일이 필요하다
2. VS Code가 사실상 Coding Tool의 표준이다
3. VS Code를 SPA 개발 표준으로 하고, Native개발은 Android Studio와 XCode를 사용한다.

## Decision

VS Code와 Android Studio, XCode를 사용한다.

## Consequences

1. VS Code와 사용할 plugin을 통일한다. (style, theme, editor config, lint, formatter 등)
2. 인터넷을 통한 설치가 아닌, 설치된 상태로 그대로 복사하여 사용할 수 있게 테스트하여 배포한다
